       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DoCalc.
       AUTHOR. Puchong Sumalanukun
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01 FirstNum     PIC 9    VALUE ZERO.
       01 SecondNum     PIC 9    VALUE ZERO.
       01 CalcResult     PIC 9    VALUE 0.
       01 UserPrompt     PIC X(39)    VALUE 
                          "Please enter two single digit numbers".
       PROCEDURE DIVISION.
       CalculaterResult.
           DISPLAY UserPrompt 
           ACCEPT FirstNum 
           ACCEPT SecondNum 
           COMPUTE CalcResult = FirstNum + SecondNum 
           DISPLAY "Result is = ", CalcResult
           STOP RUN .