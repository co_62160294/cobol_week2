       IDENTIFICATION DIVISION.
       PROGRAM-ID. CobolGreeting.
       *>Program to display COBOL greedtings
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  IterNum   PIC 9 VALUE 5.

       PROCEDURE DIVISION.
       BeginProgram.
           PERFORM  DisplayGreeting IterNum TIMES.
           STOP RUN.

       DisplayGreeting.
           DISPLAY "Greedting from COBOL".